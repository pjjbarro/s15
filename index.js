let favoriteFood = "Cordon Bleu";
let sum = 150+9;
let product = 100*90;
let isActive = true;
let favoriteRestaurants = ["Chowking","Kuya J","Seafood Island","Zarks","KFC"];
let favoriteActor = {
	firstName: "Sung",
	lastName: "Jinwoo",
	stageName: "Sung Jinwoo",
	birthDay: "August 17, 1997",
	age: 24,
	bestTVShow: null,
	bestMovie: "White Chicks",
	isActive: true
};

console.log(favoriteFood);
console.log(sum);
console.log(product);
console.log(isActive);
console.log(favoriteRestaurants);
console.log(favoriteActor);

function divideNum(num1,num2){

	console.log(num1/num2);
	return num1/num2;

};


let quotient = divideNum(50,10)

console.log(`The result of the division is: ${quotient}`)

/*
	Mini-Activity 2: 10 mins
	
	-Debug/Correct the following codes.

*/

let wrestlingCompany = "World Wrestling Entertainment";
let wrestlingCompany2 = "All Elite Wrestling";

console.log(wrestlingCompany);
console.log(wrestlingCompany2);

let worldChampion = "Big E";
console.log(worldChampion);
let universalChampion = "Roman Reigns";
console.log(universalChampion);

function printUserInfo(firstName,lastName,age){
	console.log(firstName + " " + lastName + " is " + age + " years of age.")
};

printUserInfo("Jeff","Bezos",32);

/*Lesson Proper*/
//Assignment Operators

//Basic Assignment Operator (=)
	//It allows us to assign a value to a variable.
	let variable = "initial value"

//Mathematical Operators (addition (+), subtraction (-), multiplication (*), division (/), modulo (%))

//Whenever you have used a mathematical operator, a value is returned. It is only up to us if we save that returned value.

//Addition,subtraction,multiplication,division assignment operators
	//Addition,subtraction,multiplication,division assignment operators allows us to assign the result of the operation to the value of the left operand. IT allows us to save the result of a mathematical operation to the left operand.

	let num1 = 5;
	let num2 = 10;
	let num3 = 4;
	let num4 = 40;

	//Addition Assignment Operator:
	//left operand is the variable or value of the left side of the operator
	//right operand is the variable or value of the right side of the operator
	//num1 = num1+num4 -re-assigned the value of num1 with the result of num1+num4
	num1+=num4;
	console.log(num1);//45
	num1+=55;
	console.log(num1);//100
	console.log(num4);//40 - previous right should not be affected/re-assigned.

	let string1 = "Boston";
	let string2 = "Celtics";
	string1+=string2;
	console.log(string1);//"BostonCeltics" - results in concatenation between 2 strings and saves the result in the left operand variable.
	console.log(string2);//"Celtics"

	//15+=num1; produces an error do not use assignment operator when the left operand is just data.

	//Subtraction Assignment Operator
	num1-=num2;
	console.log(num1);//90 - the result of subtraction was then re-assigned to our left operand.
	num1-=num4;
	console.log(num1);//50
	num1-=10;
	console.log(num1);//40
	num1-=string1;
	console.log(num1);//NaN - because string1 is an alphanumeric string.

	//multiplication assignment operator
	num2*=num3;
	console.log(num2);//40 - the result of multiplication was then re-assigned to our left operand.
	num2*=5;
	console.log(num2);//200 - 40*5

	//division assignment operator
	num4/=num3;
	console.log(num4);//40/4 = 10 - the result of the division was then re-assigned to the left operand.
	num4/=2;
	console.log(num4);//5

//Mathematical Operations - we follow MDAS. (Multiplication,Division,Addition,Subtraction)

	let mdasResult = 1 + 2 - 3 * 4 / 5;
	console.log(mdasResult); //.6
	/*
		1. 3*4 = 12
		2. 12/5 = 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6

	*/
//PEMDAS - Parenthesis,Exponents,Multiplication,Division,Addition,Subtraction
	
	let pemdasResult = 1 + (2-3)*(4/5);
	console.log(pemdasResult);
	/*
		1. 4/5 = .8
		2. 2-3 = -1
		3. -1 * .8 = -0.8
		4. 1 + -0.8 = .2
	*/

//Increment and Decrement
	//Increment and decrement is adding or subtracting 1 from the variable and reassigning the new value to the variable where the increment or decrement was used.

	//2 kinds of Incrementation: Pre-fix and Post-fix

	let z = 1;

	//Pre-fix Incrementation.
	++z;
	console.log(z);//2 - the value of z was added with 1 and is immediately returned.

	//Post-fix Incrementation
	z++;
	console.log(z);//3 - The value of z was added with 1
	console.log(z++);//3 - With post-incrementation the previous value of the variable is returned first before the actual incrementation.
	console.log(z);//4 - new value is now returned.

	//Pre-fix vs Post-fix Incrementation
	console.log(z++);//4 - previous value was returned at first.
	console.log(z);//5 - the new value is now returned

	console.log(++z);//6 - the new value is returned immediately.
	
	//Pre-fix and Post-fix Decrementation
	console.log(--z);//5 - with pre-fix decrementation the result of subtraction by 1 is returned immediately
	console.log(z--);//5 - with post-fix decrementation the result of subtraction by 1 is not immediately returned, instead the previous value is returned first.
	console.log(z);//4 the new value is now returned.

	//Comparison Operators

		//Comparison operators are used to compare the values of the left and right operands.
		//Comparison operators return boolean.

		//Equality or Loose Equality Operator(==)

		console.log(1 == 1);//true

		//We can also save the result of a comparison into a variable

		let isSame = 55 == 55;
		console.log(isSame);//true

		console.log(1 == '1');//true - Loose equality operator priority is the sameness of the value because with Loose Equality operator, forced coercion is done before the comparison. type coercion/forced coercion/forced conversion - JS forcibly changes the data type of the operands.
		console.log(0 == false);//true - with forced coercion, false was converted into an number and the equivalent of false into a number is 0.
		console.log(1 == true);//true - equivalent true into a number is 1.
		console.log('1'== 1);//true
		console.log(false == 0);//true
		console.log(true == "true");//false - true coerced into 1 - true coerced into number results into NaN so therefore, 1 is not equal to NaN.
		console.log("false" == false);//false - string false was coerced into a number - NaN, false being coerced into a number = 0, 0 is not equal to NaN

		/*
			With Loose comparison operator (==), values are compared and types, if operands do not have the same type, will be forced coerced/type coerced before comparison of values.

			If either operand is a number or a boolean, the operands are converted into numbers.

		*/

		console.log(true == "1");//true - true coerced into a number = 1, "1" converted into a number = 1 - 1 equals 1.
		console.log("0" == false);//true

	//Strict Equality Operator

		console.log(true === "1");//false

		//Checks both the VALUE and TYPE.

		console.log(1 === "1");//false - operands have same value but different types.

		console.log("Johnny" === "Johnny");//true - same value and same type.

		console.log(55 === "55");//false - operands have same value but different types.

		console.log("Marie" === "marie");//false - left operand is capitalized and right is small caps.

		//Inequality Operators (!=)
			//Loose inequality operators
				//Checks whether the operands are NOT equal and or have different values.
				//Will do type coercion if the operands have different types:

				console.log('1' != 1);//false
				//false = both operands were converted to numbers.
				//'1' converted into number is 1.
				//1 converted into a number is 1.
				//1 equals 1
				//not inequal.

				console.log("James" != "John");//true
				//true = "James" is not equal to "John"

				console.log(1 != true);//false
				//with type conversion: true was converted to 1
				//1 is equal to 1
				//it is NOT inequal.

				console.log(true != "true");//true
				//with type conversion: true was converted to 1
				//"true" was converted into a number but results to NaN
				// 1 is not equal to NaN
				//it IS inequal

				console.log("0" != false);//false
				//type conversion: "0" converted to 0
				//false converted to 0
				//0 is equal to 0
				//it NOT inequal

// Strict Inequality Operator (!==)
		// It will check whether the two operands have different values and will check if they have different types.

		console.log("5" !==5);
		// true - operands are inequal because they have different types.
		console.log(5 !==5);
		// false - operands are equal; they have the same value and they have the same type. 
		console.log("true" !==true); 
		// true - operands are inequal because they have different types.

		let name1 ="Juan";
		let name2 ="Shane";
		let name3 ="Peter";
		let name4 ="Jack";

		let number = 50;
		let number2 = 60;
		let numString1 = "50";
		let numString2 = "60";

		console.log(numString1 == number);
		console.log(numString1 === number);
		console.log(numString1 != number);

		// Logical Operators
		let authorization2 = isLeagalAge = && isRegistered;
		console.log(authorization2);
		let authorization3
		let requiredLevel = 95
		let requiredAge = 18

		let authorization4 = isRegistered && requiredLevel === 25;
		console.log(authorization4);
		let authorization5 = isRegistered && isLeagalAge && requiredLevel === 95;
		console.log(authorization5);

		let userName = "gamer2001";
		let userName2 = "shadow1991";
		let userAge = 15;
		let userAge2 = 30;

		let registration1 = userName.length > 8 && userAge >= requiredAge;
		// length is a property of strings which determine the number of charcters in the string.
		console.log(registration1);
		let registration2 = userName2.length > 8 && userAge2 >= requiredAge;
		console.log(registration2);

		// Or operator (|| - double pipe)
			// Or operator returns true if at least one of the operands are true.

			let userLevel = 100;
			let userLeve2 = 65;

			let guildRequirement = isRegistered && userLevel >=requiredLevel && userAge >= requiredAge;
			console.log(guildRequirement1);
			let guildRequirement2 = isRegistered || userLeve2 >= requiredLevel || userAge2 >= requiredAge;
			console.log(guildRequirement2);
			let guildRequirement3 = userLevel >= requiredLevel || userAge >= requiredAge;
			console.log(guildRequirement3);

			let guildAdmin = isAdmin || userLevel2 >= requiredLevel;
			console.log (guildAdmin);

			// Not Operator (!)
				// turns a boolean into the opposite value

				let guildAdmin2 = !isAdmin || userLevel2 >= requiredLeve;
				console.log(guildAdmin2);

				console.log(!isRegistered);
				console.log(!isLeagalAge);
				console.log(isRegistered);
				console.log(isLeagalAge);




		// If-else

			function addNum(num1,num2){

				// check if the numbers being passed arguments are number types.
				// type of keyword returns a string which tells the type of data that follows it.
				console.log(typeof num1 === "number" && typeof num2 === "number"){
					console.log("Run only if both arguments passed are number types.");
			
				console.log(num1+num2);
			} else {
				console.log("One or both of the arguments are not numbers.")
			}




			addNum(5,2);

			function login(){
				function login(username,password){
					// how can we check if the agrument passed are strings?
					if(typeof username === "string" )
				}

			// Mini- Activity

			if(username.length >= 8 && password.length >= 8){
				alert("Thank you for logging in!")

			} else if (username.length < 8){
				alert ("username is too short.")
			}else if(password.length < 8){
				alert("password is too short")
			}else {
				alert("Both username and password are too short")
			}
		} else {
			console.log("One of the arguments is not a string.");
		}

	};
		// Switch
		// Ternary
		// Activity